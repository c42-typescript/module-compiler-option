define(["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var lettersRegExp = /^[A-Za-z]+$/;
    var LettersOnlyValidator = /** @class */ (function () {
        function LettersOnlyValidator() {
        }
        LettersOnlyValidator.prototype.isAcceptable = function (s) {
            return lettersRegExp.test(s);
        };
        return LettersOnlyValidator;
    }());
    exports.LettersOnlyValidator = LettersOnlyValidator;
});
