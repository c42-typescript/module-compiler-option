define(["require", "exports", "./ZipCodeValidator", "./lettersOnlyValidator"], function (require, exports, ZipCodeValidator_1, lettersOnlyValidator_1) {
    "use strict";
    exports.__esModule = true;
    var strings = ['Hello', '98052', '101'];
    var validators = {};
    validators['ZIP Code'] = new ZipCodeValidator_1.ZipCodeValidator();
    validators['Letters Only'] = new lettersOnlyValidator_1.LettersOnlyValidator();
    strings.forEach(function (s) {
        for (var name_1 in validators) {
            console.log("\"" + s + "\" - " + (validators[name_1].isAcceptable(s) ? "matches" : "does not match") + " " + name_1);
        }
    });
});
