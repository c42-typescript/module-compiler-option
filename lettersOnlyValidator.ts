import { StringValidator } from './validation'

const lettersRegExp = /^[A-Za-z]+$/;

export class LettersOnlyValidator implements StringValidator {
    isAcceptable(s: string): boolean {
        return lettersRegExp.test(s);
    }
    
}