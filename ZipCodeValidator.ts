import { StringValidator } from "./validation";

const numberRegExp = /^[0-9]+$/;

export class ZipCodeValidator implements StringValidator {
    isAcceptable(s: string): boolean {
        return s.length === 5 && numberRegExp.test(s);
    }

}